using System;

namespace GridWar
{
	public class Sword: Weapon
	{
       public int meleePower;
        public int sword;
        Random r;
        
        public Sword()
        {
            r = new Random();
           meleePower= r.Next(3, 6);
            sword = 4;

        }
        public int MeleePowerAfterHavingSword(int meleePow)
        {
           //public int meleePower;
            return meleePower + meleePow;
        }

    }

}

