namespace GridWar
{
	public abstract class Warrior
	{
        double defense;
        double hitpoints;
        protected RandomNumberGenerator randomNumbeGenerator;
        //protected WarGrid warGrid;

        public Warrior(RandomNumberGenerator rNG=null)
        {   
            defense = 0;
            hitpoints = 100;
        }
        //public abstract class Vanquish { };
       // public abstract int meleeRangePower();
       // public abstract int magicRangePower();
        public double meleeDamage(double OppDefn, double meP)
        {
            double meD;
            meD =meP-(OppDefn * meP);
            return meD;
        }
        public double magicDamage(double OppDefn, double maP)
        {
            double maD;
            maD = maP - (OppDefn * maP);
            return maD;
        }
        
        public bool lookAtStatistics(int i, int j, int k, int l, int hitpoints)
		{
            //if(w.grids[i,j] ==1)
            //{
              bool IsMove=  move(i,j,k,l);
                return IsMove;
              bool IsVanquished = IsWarriorvanquished(hitpoints,i,j);
                return IsVanquished;
            //}
            //else
            //{
            //    return false;
            //}

		}
        public int LowestOptionToMoveXCoordinate(int m)
        {
            if (m < 0)
            {
                m++;
            }
            return m;
        }
        public int LowestOptionToMoveYCoordinate(int n)
        {
            if (n < 0)
            {
                n++;
            }
            return n;
        }
        public int HighestOptionToMoveXCoordinate(int o)
        {
            if (o > 5)
            {
                o=o-1;
            }
            return o;
        }
        public int HighestOptionToMoveYCoordinate(int p)
        {
            if (p > 5)
            {
                p--;
            }
            return p;
        }

        public bool move(int i, int j, int k, int l, WarGrid w = null)
		{
            int m, n, o, p;
            m = i - 1;
            n = j - 1;
            o = i + 1;
            p = j + 1;
            m = LowestOptionToMoveXCoordinate(m);
            n = LowestOptionToMoveYCoordinate(n);
            o = HighestOptionToMoveXCoordinate(o);
            p = HighestOptionToMoveYCoordinate(p);
            //g6int[,] temp = new int[1, 1];
           
                for (i = m; i <= o; i++)
                {
                    for (j = n; j <= p; j++)
                    {

                        if (i == k & j == l)
                        {
                        w.grids[i, j] = 0;
                        w.grids[k, l]=1;
                        return true;
                        
                        
                        }
                       
                    }
                }
            return false;
		}

        public double WarriorWhenVanquishedIncreaseDefensePercentage(double defensePercentage)
        {
            defensePercentage = defensePercentage * 1.01;
            return defensePercentage;
        }

        public double IncreaseInDefenseWhenGotAttacked(double defense, int i,int j,int k,int l)
		{
            if(move(i,j,k,l)== true & defense<1)
            {
                defense = defense * 1.0025;
                return defense;
            }
            return defense;

		}

        public double IncreaseInDefenseWhenMoved(double defense, int i, int j, int k, int l)
        {
            if (move(i, j, k, l) == true & defense < 1)
            {
                defense = defense * 1.0125;
                return defense;
            }
            return defense;
        }

		public void surrender(int i, int j, WarGrid w=null)
		{
            w.grids[i, j] = 0;
		}

		public bool IsWarriorvanquished(int hitpoint,int i, int j, WarGrid w=null)
		{
            if(hitpoint==0)
            {
                w.grids[i, j] = -1;
                return true;
            }
            return false;
		}
	}

}

