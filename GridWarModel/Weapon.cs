namespace GridWar
{
	public abstract class Weapon
	{
        WarGrid warGrid;
        Warrior warrior;
        int[,]grid;
        public Weapon()
        {
            //warGrid = new WarGrid();
            warrior = new MeleeWarrior();
            grid = new int[6, 6];
        }
		public bool checkSwordWhenfound(int [,] grid, int x,int y)
		{
            int k = grid[x, y];
            if (k==5)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkStaffWhenfound(int[,] grid, int x, int y)
        {
            int k = grid[x, y];
            if (k == 4)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }

}

