﻿using GridWar;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarGridTestingModel
{
    [TestFixture]
    public class AWarrior
    {
        public class FakeWarrior : Warrior
        {
            public FakeWarrior()
            {

            }
        }


        [Test]
        public void CanMoveFromOriginToDestinationPositionWhenWithinTheRange()
        {
            //var mockItem1 = new Mock<RandomNumberGenerator>();
            //mockItem1.SetupProperty(o => o.AmountInStock, 5);
            var sut = new FakeWarrior();
            //int[,] origin= new int[1,1];
            //int[,] destination = new int[1,1];
            //  sut.move(origin,destination, 0, 1, 1, 2);
            Assert.That(sut.move(2, 1, 5, 2), Is.EqualTo(false));

        }
        [Test]
        public void CalculateMeleeDamage()
        {
            var sut = new FakeWarrior();
            Assert.That(sut.meleeDamage(0.5, 8), Is.EqualTo(4));
        }
        //public void move(int[,] origin, int[,] destination, int i, int j, int k, int l)

        [Test]
        public void CalculateMagicDamage()
        {
            var sut = new FakeWarrior();
            Assert.That(sut.magicDamage(0.5, 8), Is.EqualTo(4));
        }

        [Test]
        public void CalculateLeastOptionInXCoordinate()
        {
            var sut = new FakeWarrior();
            Assert.That(sut.LowestOptionToMoveXCoordinate(-1), Is.EqualTo(0));
        }

        [Test]
        public void CalculateLeastOptionInYCoordinate()
        {
            var sut = new FakeWarrior();
            Assert.That(sut.LowestOptionToMoveYCoordinate(-1), Is.EqualTo(0));
        }

        [Test]
        public void CalculateHighestOptionInXCoordinate()
        {
            var sut = new FakeWarrior();
            Assert.That(sut.HighestOptionToMoveYCoordinate(6), Is.EqualTo(5));
        }

        [Test]
        public void CalculateHighestOptionInYCoordinate()
        {
            var sut = new FakeWarrior();
            Assert.That(sut.HighestOptionToMoveYCoordinate(6), Is.EqualTo(5));
        }


        //[Test]
        //public void LooksAtStatisticsIfWarriorIsVanquished()
        //{
        //    var sut = new FakeWarrior();
        //    Assert.That(sut.lookAtStatistics(1, 2, 0, 2, 80), Is.EqualTo(true));
        //}

       


        [Test]
        public void DefenseIncreasedWhenItVanquishes()
        {
            var sut = new FakeWarrior();
            Assert.That(sut.WarriorWhenVanquishedIncreaseDefensePercentage(0.2), Is.EqualTo(0.202));
        }

        //[Test]
        //public void DefenseIncreasesWhenMoved()
        //{
        //    var sut = new FakeWarrior();
        //    Assert.That(sut.IncreaseInDefenseWhenMoved(0.2, 1, 2, 1, 3), Is.EqualTo(0.2025));
        //}
        [Test]
        public void CanBeVanquished()
        {
           // var sut1 = new FakeWarGrid();
            var sut2 = new FakeWarrior();
            Assert.That(sut2.IsWarriorvanquished(10, 1, 2), Is.EqualTo(false));
        }
        [Test]
        [Category("WarriorGroup")]
        public void GroupAShouldBeCreatedWithDefinedPosition()
        {
            var sut = new WarriorGroup();
            int[,] grid = new int[0, 1];
            sut.groupA(grid, 0, 1, "melee");
            Assert.That(sut.m, Is.EqualTo(0));
            Assert.That(sut.n, Is.EqualTo(1));
        }
        [Test]
        [Category("WarriorGroup")]
        public void GroupAShouldNotBeCreatedWhenWarriorIsNotMeleeOrMagic()
        {
            var sut = new WarriorGroup();//test
            int[,] grid = new int[0, 1];
            sut.groupA(grid, 0, 1, "magic");
            Assert.That(sut.grid[0, 1], Is.EqualTo(2));

        }
        [Test]
        [Category("WarriorGroup")]
        public void GroupBShouldBeCreatedWithDefinedPosition()
        {
            var sut = new WarriorGroup();
            //var sut1 = new BattleField();
            //sut1.grids =new int [0, 1];
            int[,] grid = new int[0, 1];
            sut.groupB(grid, 5, 1, "melee");
            Assert.That(sut.m, Is.EqualTo(5));
            Assert.That(sut.n, Is.EqualTo(1));
        }
        [Test]
        [Category("WarriorGroup")]
        public void GroupBShouldNotBeCreatedWhenWarriorIsNotMeleeOrMagic()
        {
            var sut = new WarriorGroup();
            int[,] grid = new int[0, 1];
            sut.groupB(grid, 0, 1, "ma");
            Assert.That(sut.grid[0, 1], Is.EqualTo(0));

        }

        //[Test]
        //public void IsInitlializedWithMagicPower()
        //{
        //    var mockitem = new Mock<RandomNumberGenerator>();
        //    mockitem.SetupProperty(o => o.RandomNumberGeneratorInRange(5, 10), 6);

        //    var sut = new MeleeWarrior(mockitem.Object);
        //    Assert.That(sut.meP, Is.EqualTo(6));
        //}

        [Test]
        [Category("Melee")]
        public void MeleePowerIsIncreasedWhenItVanquishedWarrior()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.MeleeWarriorMeleePowerIncreaseWhenVanquishesWarrior(1), Is.EqualTo(1.01));
        }

        [Test]
        [Category("Melee")]
        public void MagicPowerIsIncreasedWhenItVanquishedWarrior()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.MeleeWarriorMagicPowerIncreaseWhenVanquishesWarrior(3), Is.EqualTo(3.0075));

        }

        [Test]
        [Category("Melee")]
        public void HasMeleePowerIncreasedWhenMeleeWarriorAttacks()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.IncreaseInMeleePowerWhenMeleeWarriorAttacks(1), Is.EqualTo(1.005));
        }

        [Test]
        [Category("Melee")]
        public void HasMagicPowerIncreasedWhenMeleeWarriorAttacks()
        {
            var sut = new MeleeWarrior();
            Assert.That(sut.IncreaseInMagicPowerWhenMeleeWarriorAttacks(3), Is.EqualTo(3.00375));
        }

        //[Test]
        //public void IsInitlializedWithMagicPower()
        //{
        //    var mockitem = new Mock<RandomNumberGenerator>();
        //    mockitem.SetupProperty(o => o.RandomNumberGeneratorInRange(5, 10), 6);

        //    var sut = new MeleeWarrior(mockitem.Object);
        //    Assert.That(sut.meP, Is.EqualTo(6));
        //}

        [Test]
        [Category("Magic")]
        public void MagicMeleePowerIsIncreasedWhenItVanquishedWarrior()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.MagicWarriorMeleePowerIncreaseWhenVanquishesWarrior(1), Is.EqualTo(1.0025));
        }

        [Test]
        [Category("Magic")]
        public void MagicWarriorMagicPowerIsIncreasedWhenItVanquishedWarrior()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.MagicWarriorMagicPowerIncreaseWhenVanquishesWarrior(1), Is.EqualTo(1.01));

        }

        [Test]
        [Category("Magic")]
        public void HasMeleePowerIncreasedWhenMagicWarriorAttacks()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.IncreaseInMeleePowerWhenMagicWarriorAttacks(1), Is.EqualTo(1.00125));
        }


        [Test]
        [Category("Magic")]
        public void MagicWarriorHasMagicPowerIncreasedWhenMeleeWarriorAttacks()
        {
            var sut = new MagicWarrior();
            Assert.That(sut.IncreaseInMagicPowerWhenMagicWarriorAttacks(1), Is.EqualTo(1.005));
        }
    }
}
